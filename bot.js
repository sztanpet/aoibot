var util = require("util"),
    repl = require("repl"),
    irc  = require("./irc"),
    http = require("http");

BOTDEBUG = false;
//BOTDEBUG = true;
l("set BOTDEBUG = true for extra info\n");
repl.start("aoibot> ");

function l( text ) {
  util.log( text );
}

function d() {
  
  if ( BOTDEBUG )
    for( var i = 0, j = arguments.length; i < j; i++ )
      util.log( util.inspect( arguments[i] ) );
  
}

var config = {
  boardhost:    "aoimirror.adaba.hu",
  imgpostpath:  "/board/post.php",
  linkpostpath: "/board/post_link.php",
  realname:     "msg sztanpet for more info",
  username:     "aoibot",
  nick:         "aoibot",
  altnick:      "aoibot#",
  channels: [
    "#aoianime"
  ],
  servers: [
    "ircnet.eversible.com",
    "ircnet.club-internet.fr",
    "ircnet.nerim.fr",
    "irc1.fr.ircnet.net"
  ]
};

var onMessage = function( msg ) {

  if ( this.state == "waitingfor005" )
    return;
  
  if ( msg.prefix && msg.command == "PRIVMSG" )
    extractInfo.apply( this, [ msg ] );

};

var extractInfo = function( msg ) {
  var nick = ( msg.prefix.match(/^(\S+)!.*$/) || [] )[1];
  var urls = msg.arguments[ msg.lastarg ].match(/https?:\/\/\S+/gi);
  
  if ( !urls )
    return;

  if ( urls.length == 1 )
    var comment = msg.arguments[ msg.lastarg ].replace( urls[ 0 ], "" );
  else
    var comment = false;

  for ( var i = 0, j = urls.length; i < j; i++ )
    handleURL.apply( this, [ urls[ i ], nick, comment ] );

};

var parseuri = new RegExp("https?:\/\/(.*?)(?:\:(\\d+?))?(\/.*)");
var handleURL = function( url, nick, comment ) {
  var uri  = parseuri.exec( url );
  var self = this;

  if ( !uri )
    return;

  var httpclient = http.createClient( uri[2] || 80, uri[1] );
  var request = httpclient.request('GET', uri[3], {
    "Host": uri[1] + ( ( uri[2] )? uri[2]: "" ),
    "Referer": url,
    "User-Agent": "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/532.0 (KHTML, like Gecko) Chrome/4.0.211.2 Safari/532.0",
    "Connection": "close"
  });
  request.end();
  request.on('response', function( response ) {

    response.connection.destroy();

    if ( response.statusCode != 200 && response.statusCode != 301 && response.statusCode != 302 ) //something went wrong TODO handle it
      return d("UNKNOWN STATUSCODE, dumping request", url, response );

    if ( !response.headers["content-type"] || response.headers["content-type"].indexOf("text/") == 0 )
      var path = config.linkpostpath;
    else if ( !response.headers["content-type"] || response.headers["content-type"].indexOf("image/") == 0 )
      var path = config.imgpostpath;

    if ( !path )
      return d("unknown content type, skipping: " + url, response.headers );

    var postclient = http.createClient(80, config.boardhost );
    path = path + "?url=" + encodeURI( url ) + "&nick=" + encodeURI( nick ) + ( ( comment )? "&comment=" + encodeURI( comment ) : "" );

    l("sending request for:\n\t" + path + "\n");
    
    var postrequest = postclient.request("GET", path, {
      "Host": config.boardhost,
      "Connection": "close"
    });

    postrequest.end();

  });

};

client = new irc.client( config );
client.on("messagein", onMessage );
client.d = d; // monkeypatch the log functions
client.l = l;
