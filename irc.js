var net  = require("net"),
    util = require("util");

exports.client = function( options ) {
  var self = this;

  self.options = options || {};
  self.options.port     = options.port || 6667;
  self.options.password = options.passsword || false;
  self.options.encoding = options.encoding || "utf8";
  self.options.username = options.username || "nodeirc";
  self.options.realname = options.realname || "node.js irc client";
  self.options.nick     = options.nick || "nodejs";
  self.options.altnick  = options.altnick || "nodejs#";
  self.options.channels = options.channels || [];
  self.options.timeout  = options.timeout || 3 * 60 * 60 * 1000;
  self.options.servers  = options.servers || [];
  self.options.autoreconnect = options.autoreconnect || true;
  self.state    = "";
  self.readbuff = "";
  self._events  = {};
  self.debug    = false;
  
  if ( self.options.servers.length == 0 )
    throw new Error("No servers specified");
  
  self.currentserver = self.options.servers[0];
  self.connectTo( self.currentserver );
  
};

var proto = exports.client.prototype;
proto.connectTo = function( server ) {
  var self  = this,
      match = server.match(/(.*):(\d+)/),
      port  = 6667,
      host  = server;
  
  if ( match ) {
    
    host = match[1];
    port = match[2];
    
  }
  
  self.state = "notconnected";
  self.l("Making connection to " + host + ":" + port );
  self.socket = new net.createConnection( port, host );
  self.socket.setEncoding( self.options.encoding );
  self.socket.setTimeout( self.options.timeout );
  
  self.socket.on("connect", function() {
    self.d("Connected", arguments );
    self.onConnect.apply( self, arguments );
  });
  
  self.socket.on("data", function() {
    self.d("Received data", arguments );
    self.onData.apply( self, arguments );
  });
  
  self.socket.on("timeout", function() {
    self.d("Got timeout event", arguments );
    self.onDisconnect.apply( self, arguments );
  });
  
  self.socket.on("end", function() {
    self.d("Got end event", arguments );
    self.onDisconnect.apply( self, arguments );
  });
  
  self.socket.on("error", function() {
    self.d("Got error event", arguments );
    self.onDisconnect.apply( self, arguments );
  });
  
  self.socket.on("timeout", function() {
    self.d("Got timeout event", arguments );
    self.onDisconnect.apply( self, arguments );
  });
  
  self.socket.on("close", function() {
    self.d("Got close event", arguments );
    self.onDisconnect.apply( self, arguments );
  });
  
  self._timeout = setTimeout(function() {
    self.onDisconnect.apply( self, ["pingtimeout"] );
    self.state = "disconnected";
    self.socket.close();
  }, 60 * 1000);
  
}

proto.send = function( data ) {

  this.d("Readystate", this.socket.readyState, "bot state", this.state, "data", data );
  
  if ( this.state == "disconnected" || this.socket.readyState != "open" )
    return;

  // TODO flood prot
  this.socket.write( data, this.options.encoding );

};

proto.close = function() {
  return this.socket.destroy();
};

proto.addListener = function( event, fun ) {

  if ( !this._events[ event ] )
    this._events[ event ] = [];

  this._events[ event ].push( fun );

};
proto.on = proto.addListener;

proto.emit = function( event ) {

  if ( !event || !this._events[ event ] )
    return;

  var args = [];
  for ( var i = 1, j = arguments.length; i < j; i++ )
    args.push( arguments[ i ] );

  for ( var i = 0, j = this._events[ event ].length; i < j; i++ )
    this._events[ event ][ i ].apply( this, args );

};

proto.isOn = function( channel ) {

  if ( !channel )
    return this.options.channels;

  for ( var i = 0, j = this.options.channels.length; i < j; i++ )
    if ( this.options.channels[ i ] == channel )
      return true;

  return false;
};

proto.onConnect = function() {
  var data   = [];
  this.state = "waitingfor005";

  this.emit("connect");

  if ( this.options.password )
    data.push("PASS :" + this.options.password );

  data.push("NICK " + this.options.nick );
  data.push("USER " + this.options.username + " \"asd\" localhost :" + this.options.realname );
  data.push(""); //for those two final \r\n
  data.push("");

  this.send( data.join("\r\n") );

};

proto.onDisconnect = function( err ) {
  
  if ( this.state == "disconnected" )
    return;
  
  this.l("Disconnected from " + this.currentserver );
  
  this.state = "disconnected";
  try {
    clearInterval( this._timer );
    clearTimeout( this._timeout );
  } catch( e ) {}

  this.emit("disconnect", err );
  
  if ( this.socket && this.socket.fd )
    this.socket.destroy();
  
  if ( !this.options.autoreconnect )
    return;
  
  var found        = false,
      self         = this,
      currentindex = 0;
  
  this.options.servers.forEach( function( value, index, servers ) {
    
    if ( found === false && this.currentserver != value && index > currentindex ) {
      
      found = true;
      this.currentserver = value;
      
    } else if ( this.currentserver == value )
      currentindex = index;
    
  }, this );
  
  if ( !found && ( this.options.servers.length - 1 ) == currentindex )
    this.currentserver = this.options.servers[0];
  
  setTimeout( function() {
    self.connectTo( self.currentserver );
  }, 10 * 1000 );
  
};

proto.onData = function( data ) {

  if ( this.readbuff != "") {

    data = this.readbuff + data;
    this.readbuff = "";

  }

  if ( !this.linebreak ) {

    if ( /\r\n/.test( data ) )
      this.linebreak = new RegExp("\r\n");

    else if ( /\n/.test( data ) )
      this.linebreak = new RegExp("\n");

  }

  // TODO be more resilient/agressive about invalid input (timer for if we dont get something to parse in X seconds, close the socket and bail)

  if ( !this.linebreak )
    throw new Error("no linebreaks in data, might not be so serious");

  var tmp = data.split( this.linebreak.source );

  if ( tmp[ tmp.length - 1 ] != "") //did not get a full line, buffer the last line, parse the first few
    this.readbuff += tmp.pop();

  for( var i = 0, j = tmp.length; i < j; i++ ) {

    if ( tmp[ i ] == "" )
      continue;

    var msg = this.parse( tmp[ i ] );
    
    if ( !msg )
      throw new Error("unable to parse message");

    if ( msg.command == "PING" ) {
      var data = "PONG";

      if ( msg.arguments.length > 0 )
        data += " :" + msg.arguments[0];

      this.send( data + "\r\n" );
    }

    if ( msg.command == "PONG" )
      clearTimeout( this._timeout );

    if ( this.state == "waitingfor005" && msg.command == "001" )
      this.prefix = msg.arguments[1].match(/Relay Network (.*)/)[1];

    if ( this.state == "waitingfor005" && msg.command == "433" ) { //nickname already in use
      var newnick = this.options.altnick.replace('#', Math.floor( Math.random() * 8 ) + 1);
      var data = "NICK " + newnick;

      this.send( data + "\r\n" );

    }

    if ( this.state == "waitingfor005" && msg.command == "376" ) { //376 end of motd, that should be safe to look for
      this.state = "connected";
      var self = this;

      clearTimeout( self._timeout );
      self._timer = setInterval(function() {

        self._timeout = setTimeout(function() {
          
          clearInterval( self._timer );
          self.state = "disconnected";
          self.socket.destroy();
          self.onDisconnect.apply( self, ["pingtimeout"] );
          
        }, 5 * 1000);

        self.send("PING :lofasz\r\n");
      }, 15 * 1000);

      this.emit("connected");

      for ( var i = 0, j = this.options.channels.length; i < j; i++ )
        this.send("JOIN " + this.options.channels[ i ] + "\r\n");

    }

    if ( msg.command == "JOIN")
      if ( !this.isOn( msg.arguments[0] ) )
        this.options.channels.push( msg.arguments[0] );

    if ( msg.command == "PART")
      for ( var i = 0, j = this.options.channels.length; i < j; i++ )
        if ( this.options.channels[ i ] == msg.arguments[0] && this.isOn( msg.arguments[0] ) )
          this.options.channels.splice( i, 1);

    if ( msg.command == "NICK" && msg.prefix == this.prefix ) {

      this.prefix = msg.arguments[0] + this.prefix.slice( this.prefix.indexOf("!") );
      this.options.nick = msg.arguments[0];

    }

    this.emit("messagein", msg );

  }

};

proto.parse = function( text ) {

  if ( typeof( text ) != "string" )
    return false;
  
  var tmp = text.split(" ");

  if ( tmp.length < 2 )
    return false;
  
  var prefix    = null;
  var command   = null;
  var lastarg   = null;
  var arguments = [];
  
  for ( var i = 0, j = tmp.length; i < j; i++ ) {
    
    if ( i == 0 && tmp[ i ].indexOf(":") == 0 )
      prefix = tmp[0].substr(1);

    else if ( tmp[ i ] == "" )
      continue;

    else if ( !command && tmp[ i ].indexOf(":") != 0 )
      command = tmp[ i ].toUpperCase();

    else if ( tmp[ i ].indexOf(":") == 0 ) {

      tmp[ i ] = tmp[ i ].substr(1);
      tmp.splice( 0, i );
      arguments.push( tmp.join(" ") );
      lastarg = arguments.length - 1;
      break;

    } else
      arguments.push( tmp[ i ] );
    
  }
  
  return {
    prefix: prefix,
    command: command,
    arguments: arguments,
    lastarg: lastarg,
    orig: text
  };

};

proto.l = function( text ) {
  util.log( util.inspect( text ) );
}

proto.d = function() {
  
  if ( this.debug )
    for( var i = 0, j = arguments.length; i < j; i++ )
      util.log( util.inspect( arguments[i] ) );
  
}